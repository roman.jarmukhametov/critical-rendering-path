/**
 * Main entry point for the application.
 *
 * This script is responsible for importing necessary stylesheets and initializing
 * specific modules when the DOM is fully loaded. It ensures that the application's
 * UI components, such as the "Join Our Program" subscription section and the
 * "Big Community of People Like You" section, are correctly set up and displayed to the user.
 */

// Imports the main stylesheet for the application.
// This import statement allows Webpack (or a similar bundler) to process and include
// the stylesheet in the build output.
import "../styles/style.css";

// Imports the JoinUsSection module from the join-us-section.js file.
// This module is responsible for creating and displaying the "Join Our Program"
// section of the application, including the subscription form.
import { JoinUsSection } from "./join-us-section.js";

// Imports the CommunitySection module from the community-cards-section.js file.
// This module is responsible for fetching and displaying the community member cards
// in the "Big Community of People Like You" section of the application.
import { CommunitySection } from "./community-cards-section.js";

/**
 * Adds an event listener for the 'DOMContentLoaded' event to ensure that both
 * the JoinUsSection and CommunitySection modules are only initialized once the
 * DOM is fully loaded. This setup step is crucial for displaying the respective
 * sections to the user and prevents any attempts to manipulate DOM elements before
 * they are available.
 */
document.addEventListener("DOMContentLoaded", function () {
  // Initializes the "Join Our Program" section by creating and inserting
  // it into the DOM. This action is performed by the JoinUsSection module.
  JoinUsSection.init();

  // Initializes the "Big Community of People Like You" section by fetching
  // community member data and displaying it. This action is performed by
  // the CommunitySection module.
  CommunitySection.init();
});
