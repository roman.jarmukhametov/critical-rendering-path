// Imports the email validation function from the email-validator module.
import { validate } from "./email-validator.js";

// Imports utility functions for managing subscription status in local storage.
import {
  getSubscriptionStatus,
  setSubscriptionStatus,
  removeSubscriptionStatus,
} from "./local-storage-util.js";

// Imports the subscribeUser functions from the subscription-service module.
import { subscribeUser } from "./subscription-service.js";

// Imports the unsubscribeUser function from the unsubscribe-service module.
import { unsubscribeUser } from "./unsubscribe-service.js";

/**
 * Creates and returns a subscription form element with email input and submit button.
 * It also sets up the form's event listener for the submit event.
 *
 * @returns {HTMLElement} The subscription form element.
 */
function createSubscriptionForm() {
  // Creates the form element and sets its class and action attributes.
  const form = document.createElement("form");
  form.className = "app-section__form";
  form.action = "#";

  // Creates the email input field, setting its type, placeholder, and class.
  const emailInput = document.createElement("input");
  emailInput.type = "email";
  emailInput.placeholder = "Email";
  emailInput.className = "app-section__input--email";

  // Creates the submit button, setting its class, type, and value attributes.
  const submitButton = document.createElement("input");
  submitButton.className = "app-section__button app-section__button--subscribe";
  submitButton.type = "submit";
  submitButton.value = "Subscribe";

  // Appends the email input and submit button to the form element.
  form.appendChild(emailInput);
  form.appendChild(submitButton);

  // Attaches an event listener to handle form submission.
  form.addEventListener("submit", (event) => {
    event.preventDefault(); // Prevents the default form submission.
    handleFormSubmit(emailInput, submitButton);
  });

  // Adjusts the form UI based on the current subscription status.
  adjustFormUI(form, emailInput, submitButton);

  return form; // Returns the fully constructed form element.
}

/**
 * Handles the form submission event. Validates the email and updates the UI and local storage based on the validation result.
 *
 * @param {HTMLInputElement} emailInput - The email input element of the form.
 * @param {HTMLInputElement} submitButton - The submit button element of the form.
 */

function handleFormSubmit(emailInput, submitButton) {
  // Disable the button and set the opacity to indicate processing.
  const setButtonState = (disabled) => {
    submitButton.disabled = disabled;
    submitButton.style.opacity = disabled ? "0.5" : "1";
  };

  // Validates the entered email address.
  const isValidEmail = validate(emailInput.value);

  // Attempts to remove any existing error message from previous submissions.
  const existingErrorMsg = document.querySelector(".error-message");
  if (existingErrorMsg) {
    existingErrorMsg.remove();
  }

  // Determine the action based on the button's value.
  const action = submitButton.value.toLowerCase();

  // Handle the subscribe action.
  if (action === "subscribe") {
    if (isValidEmail) {
      setButtonState(true); // Disable and apply opacity to the button.
      subscribeUser(emailInput.value)
        .then((data) => {
          // Handle success or failure from the subscription attempt.
          if (data.success) {
            setSubscriptionStatus(true, emailInput.value);
            adjustFormUI(emailInput.form, emailInput, submitButton);
            console.log("Subscription successful:", data);
          } else {
            console.error("Subscription failed with message:", data.error);
          }
        })
        .catch((error) => {
          console.error("Subscription failed:", error);
        })
        .finally(() => {
          setButtonState(false); // Re-enable and reset opacity of the button.
        });
    } else {
      emailInput.className = "error-message"; // Add an error class to the email input.
      // Display an error message for an invalid email.
    }
  }
  // Handle the unsubscribe action.
  else if (action === "unsubscribe") {
    setButtonState(true); // Disable and apply opacity to the button.
    unsubscribeUser()
      .then((data) => {
        // Handle success or failure from the unsubscription attempt.
        if (data.success) {
          removeSubscriptionStatus();
          adjustFormUI(emailInput.form, emailInput, submitButton);
          console.log("Unsubscription successful:", data);
        } else {
          console.error("Unsubscription failed with message:", data.error);
        }
      })
      .catch((error) => {
        console.error("Unsubscription failed:", error);
      })
      .finally(() => {
        setButtonState(false); // Re-enable and reset opacity of the button.
      });
  }
}

// Only proceed if the email is valid.

/**
 * Adjusts the form's UI based on the subscription status, hiding the email input and changing the submit button text if subscribed.
 *
 * @param {HTMLFormElement} form - The form element containing the email input and submit button.
 * @param {HTMLInputElement} emailInput - The email input element of the form.
 * @param {HTMLInputElement} submitButton - The submit button element of the form.
 */
function adjustFormUI(form, emailInput, submitButton) {
  // Ensure that the elements exist.
  if (!form || !emailInput || !submitButton) {
    console.error("One or more UI elements are missing.");
    return; // Exit the function if elements are not found.
  }

  // Checks if the user is currently subscribed.
  const isSubscribed = getSubscriptionStatus();

  if (isSubscribed) {
    emailInput.style.display = "none"; // Hides the email input field.
    submitButton.value = "Unsubscribe"; // Changes the submit button text to indicate an unsubscribe action.
    form.style.justifyContent = "center"; // Centers the unsubscribe button in the form.
  } else {
    emailInput.value = ""; // Clears the email input field.
    emailInput.style.display = ""; // Shows the email input field.
    submitButton.value = "Subscribe"; // Changes the submit button text to indicate a subscribe action.
    form.style.justifyContent = ""; // Resets the form to default styling.
  }
}

// Exports the createSubscriptionForm function to make it available for import in other modules.
export { createSubscriptionForm };
