import { describe, it, expect, beforeEach, afterEach, vi } from "vitest";
import {
  validate,
  validateAsync,
  validateWithThrow,
  validateWithLog,
} from "./email-validator";

describe("validate", () => {
  // Test with valid email addresses
  it("should validate emails with domains in the VALID_EMAIL_ENDINGS array", () => {
    const validEmails = [
      "user@gmail.com",
      "user@outlook.com",
      "user@yandex.ru",
    ];
    validEmails.forEach((email) => {
      expect(validate(email)).toBe(true);
    });
  });

  // Test with invalid email addresses
  it("should not validate emails with domains not in the VALID_EMAIL_ENDINGS array", () => {
    const invalidEmails = [
      "user@invalid.com",
      "user@example.org",
      "user@nonexistent.co",
    ];
    invalidEmails.forEach((email) => {
      expect(validate(email)).toBe(false);
    });
  });

  // Test with an empty string
  it("should not validate an empty string", () => {
    expect(validate("")).toBe(false);
  });

  // Test with a malformed email address
  it("should not validate malformed email addresses", () => {
    const malformedEmails = [
      "useratgmail.com",
      "user@gmailcom",
      "user@.com",
      "@gmail.com",
    ];
    malformedEmails.forEach((email) => {
      expect(validate(email)).toBe(false);
    });
  });

  // Test with a null or undefined value
  it("should not validate null or undefined values", () => {
    expect(validate(null)).toBe(false);
    expect(validate(undefined)).toBe(false);
  });

  // Test with case sensitivity
  it("should be case-insensitive for the domain part", () => {
    const email = "user@GMAIL.com";
    expect(validate(email)).toBe(true);
  });

  // Test with leading/trailing whitespace
  it("should trim the email before validation", () => {
    const email = "   user@gmail.com   ";
    expect(validate(email)).toBe(true);
  });

  // Test with a domain that has a valid ending as a subdomain
  it("should not validate when the valid domain is a subdomain", () => {
    const email = "user@sub.gmail.com";
    expect(validate(email)).toBe(false);
  });

  // Test with an email that has additional characters after the domain
  it("should not validate emails with additional characters after the domain", () => {
    const email = "user@gmail.com/something";
    expect(validate(email)).toBe(false);
  });
});

describe("validateAsync", () => {
  // Test with valid email addresses
  it("should validate emails with domains in the VALID_EMAIL_ENDINGS array", async () => {
    const validEmails = [
      "user@gmail.com",
      "user@outlook.com",
      "user@yandex.ru",
    ];
    for (const email of validEmails) {
      await expect(validateAsync(email)).resolves.toBe(true);
    }
  });

  // Test with invalid email addresses
  it("should not validate emails with domains not in the VALID_EMAIL_ENDINGS array", async () => {
    const invalidEmails = [
      "user@invalid.com",
      "user@example.org",
      "user@nonexistent.co",
    ];
    for (const email of invalidEmails) {
      await expect(validateAsync(email)).resolves.toBe(false);
    }
  });

  // Test with an empty string
  it("should not validate an empty string", async () => {
    await expect(validateAsync("")).resolves.toBe(false);
  });

  // Test with a malformed email address
  it("should not validate malformed email addresses", async () => {
    const malformedEmails = [
      "useratgmail.com",
      "user@gmailcom",
      "user@.com",
      "@gmail.com",
    ];
    for (const email of malformedEmails) {
      await expect(validateAsync(email)).resolves.toBe(false);
    }
  });

  // Test with a null or undefined value
  it("should not validate null or undefined values", async () => {
    await expect(validateAsync(null)).resolves.toBe(false);
    await expect(validateAsync(undefined)).resolves.toBe(false);
  });

  // Test with case sensitivity
  it("should be case-insensitive for the domain part", async () => {
    const email = "user@GMAIL.com";
    await expect(validateAsync(email)).resolves.toBe(true);
  });

  // Test with leading/trailing whitespace
  it("should trim the email before validation", async () => {
    const email = "   user@gmail.com   ";
    await expect(validateAsync(email)).resolves.toBe(true);
  });

  // Test with a domain that has a valid ending as a subdomain
  it("should not validate when the valid domain is a subdomain", async () => {
    const email = "user@sub.gmail.com";
    await expect(validateAsync(email)).resolves.toBe(false);
  });

  // Test with an email that has additional characters after the domain
  it("should not validate emails with additional characters after the domain", async () => {
    const email = "user@gmail.com/something";
    await expect(validateAsync(email)).resolves.toBe(false);
  });
});

describe("validateWithThrow", () => {
  // Test with valid email addresses
  it("should return true for valid emails", () => {
    const validEmails = [
      "user@gmail.com",
      "user@outlook.com",
      "user@yandex.ru",
    ];
    validEmails.forEach((email) => {
      expect(() => validateWithThrow(email)).not.toThrow();
    });
  });

  // Test with invalid email addresses
  it("should throw an error for emails with domains not in the VALID_EMAIL_ENDINGS array", () => {
    const invalidEmails = [
      "user@invalid.com",
      "user@example.org",
      "user@nonexistent.co",
    ];
    invalidEmails.forEach((email) => {
      expect(() => validateWithThrow(email)).toThrow(
        "The provided email is invalid."
      );
    });
  });

  // Test with an empty string
  it("should throw an error for an empty string", () => {
    expect(() => validateWithThrow("")).toThrow(
      "The provided email is invalid."
    );
  });

  // Test with a malformed email address
  it("should throw an error for malformed email addresses", () => {
    const malformedEmails = [
      "useratgmail.com",
      "user@gmailcom",
      "user@.com",
      "@gmail.com",
    ];
    malformedEmails.forEach((email) => {
      expect(() => validateWithThrow(email)).toThrow(
        "The provided email is invalid."
      );
    });
  });

  // Test with a null or undefined value
  // Note: This will require the function to handle null and undefined as input,
  // which may necessitate slight modification to the function to avoid TypeErrors.
  it("should throw an error for null or undefined values", () => {
    expect(() => validateWithThrow(null)).toThrow(
      "The provided email is invalid."
    );
    expect(() => validateWithThrow(undefined)).toThrow(
      "The provided email is invalid."
    );
  });

  // Test with case sensitivity
  it("should return true for valid emails regardless of case", () => {
    const email = "user@GMAIL.com";
    expect(() => validateWithThrow(email)).not.toThrow();
  });

  // Test with leading/trailing whitespace
  it("should return true for valid emails with leading/trailing whitespace", () => {
    const email = "   user@gmail.com   ";
    expect(() => validateWithThrow(email)).not.toThrow();
  });

  // Test with a domain that has a valid ending as a subdomain
  it("should throw an error when the valid domain is a subdomain", () => {
    const email = "user@sub.gmail.com";
    expect(() => validateWithThrow(email)).toThrow(
      "The provided email is invalid."
    );
  });

  // Test with an email that has additional characters after the domain
  it("should throw an error for emails with additional characters after the domain", () => {
    const email = "user@gmail.com/something";
    expect(() => validateWithThrow(email)).toThrow(
      "The provided email is invalid."
    );
  });
});

describe("validateWithLog", () => {
  // Mock console.log before each test
  let consoleSpy;
  beforeEach(() => {
    consoleSpy = vi.spyOn(console, "log").mockImplementation();
  });

  // Restore the original console.log after each test
  afterEach(() => {
    consoleSpy.mockRestore();
  });

  // Test with a valid email
  it("should return true for valid emails and log the result", () => {
    const email = "user@gmail.com";
    expect(validateWithLog(email)).toBe(true);
    expect(consoleSpy).toHaveBeenCalledWith(
      `Validation result for "${email}": true`
    );
  });

  // Test with an invalid email
  it("should return false for invalid emails and log the result", () => {
    const email = "user@invalid.com";
    expect(validateWithLog(email)).toBe(false);
    expect(consoleSpy).toHaveBeenCalledWith(
      `Validation result for "${email}": false`
    );
  });

  // Additional tests can follow the same pattern
  // Test with an empty string
  it("should return false for an empty string and log the result", () => {
    const email = "";
    expect(validateWithLog(email)).toBe(false);
    expect(consoleSpy).toHaveBeenCalledWith(
      `Validation result for "${email}": false`
    );
  });

  // Test with a valid domain in uppercase (to check case insensitivity)
  it("should be case-insensitive for the domain part and log the result", () => {
    const email = "user@GMAIL.COM";
    expect(validateWithLog(email)).toBe(true);
    expect(consoleSpy).toHaveBeenCalledWith(
      `Validation result for "${email}": true`
    );
  });
});
